$(document).ready(function () {

    /*$('.topslider').bxSlider(); Remove comments to add back topslider functionality for multiple slides*/

    $('.carslider').bxSlider({ mode: 'fade' });

    $('.bottomslider').bxSlider({
        pagerCustom: '#thumbnails'
    });


    //Tracking
    $('.bx-prev').click(function () {
        _gaq.push(['_trackEvent', 'Model Carousel', 'Click', 'Left']);
    })


    $('.bx-next').click(function () {
        _gaq.push(['_trackEvent', 'Model Carousel', 'Click', 'Right']);
    })


    $("#thumbnails li a").hover(function () {
        $(this).next("img").animate({ opacity: "show" }, "fast");
        $(this).css("color", "#077dbf");
    }, function () {
        $(this).next("img").animate({ opacity: "hide" }, "fast");
        $(this).css("color", "#ffffff");
    });

    var ismobile = false;
    var ua = navigator.userAgent.toLowerCase();
    if (/android|webos|iphone|ipad|ipod|blackberry/i.test(ua)) {
        ismobile = true;
        $('#slide3 .bx-wrapper .bx-controls-direction a').css({ display: "block" });
        $('#bottom-wrap').css({ display: "none" });

    }

    $('.scroll-down').click(function(){
        $('html, body').animate({
            scrollTop: $( $(".carslider")).offset().top
        }, 500);
    });

});

